package com.leewise.counseling.Service;

import com.leewise.counseling.Repository.CustomerRepository;
import com.leewise.counseling.entity.Customer;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CustomerService {
    private final CustomerRepository customerRepository;
    //컨트롤러에서 접근하지 못하게끔 하기 위해, private를 적용한다.

    public void setCustomer(String name , String phone) {
        Customer addData = new Customer();
        addData.setName(name);
        addData.setPhone(phone);

        customerRepository.save(addData);
    }
}
