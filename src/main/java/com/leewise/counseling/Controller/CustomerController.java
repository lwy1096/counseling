package com.leewise.counseling.Controller;

import com.leewise.counseling.Model.CustomerRequest;
import com.leewise.counseling.Service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/customer")
public class CustomerController {
    private final CustomerService customerService ;
@PostMapping("/data")
    public String setCustomer(@RequestBody CustomerRequest request) {
        customerService.setCustomer(request.getName(), request.getPhone());
        return "완료되었습니다.";
    }
}
